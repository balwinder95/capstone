process.env.NODE_ENV = 'localDev';

config = require('config');
var express = require("express");
var logfmt = require("logfmt");
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var errorhandler = require('errorhandler');
var logger = require('morgan');
var methodOverride = require('method-override');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var args = process.argv;
// if (!args[2]) {
//     ApplicationSettings.ENV = 'development';
// } else {
//     ApplicationSettings.ENV = args[2];
// }

console.log("Using envoirnment " + process.env.NODE_ENV);

var mobile = require('./routes/mobile');
mysqlLib = require('./routes/mysql');


var app = express();

// all environments
app.set('port', process.env.PORT || config.PORT);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
//app.use(favicon(__dirname + '/views/download.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(logfmt.requestLogger());


app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// development only
if ('development' == app.get('env')) {
    app.use(errorhandler());
}

app.get('/test', function (req, res) {
    res.render('test');
});

app.post('/signup', mobile.signup);
app.post('/user_login', mobile.userLogin);
app.post('/change_password', mobile.changePassword);
app.post('/search_app', mobile.searchApp);
// app.post('/data_usage', admin.dataUsage);
// app.post('/cpu_graph', admin.cpuGraph);
// app.post('/data_graph', admin.dataGraph);

http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port " + app.get('port'));
});