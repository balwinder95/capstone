var func = require('./commonfunction');
var constants = require('./constants');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var async = require('async');


exports.signup = function (req, res) {

    var username = req.body.username;
    var password = req.body.password;

    var manValues = [username, password];

    async.waterfall ([
        function (callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        }, function (callback) {
            var sql = "SELECT `user_id` FROM `users` WHERE `username`=?";
            connection.query(sql, [username], function (err, userResponse) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                } else if (userResponse.length > 0) {
                    sendResponse.sendSuccessData("Username already exists.", [], res);
                } else {
                    callback();
                }
            })
        }], function() {

            password = md5(password);

            var sql = "INSERT INTO `users` (`username`,`password`) VALUES(?,?)";
            connection.query(sql, [username, password], function (err, response) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                } else {
                    sendResponse.sendSuccessData("Account created successfully.", [], res);
                }
            });
        });

};



exports.userLogin = function (req, res) {

	var username = req.body.username;
	var password = req.body.password;

	var manValues = [username, password];

	async.waterfall ([
		function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        }], function () {

        	password = md5(password);

        	var sql = "SELECT `user_id` FROM `users` WHERE `username`=? AND `password`=?";
        	connection.query(sql, [username, password], function (err, response) {

        		if (err) {
        			sendResponse.somethingWentWrongError(res);
        		} else if (response.length > 0) {
        			sendResponse.sendSuccessData("Login successful.", [], res);
        		} else {
        			sendResponse.sendSuccessData("Incorrect username or password.", [], res);
        		}
        	});

        });

};


exports.changePassword = function (req, res) {

	var username = req.body.username;
	var oldPassword = req.body.old_password;
	var newPassword = req.body.new_password;

	var manValues = [username, oldPassword, newPassword];

	async.waterfall ([
		function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        }], function () {

        	oldPassword = md5(oldPassword);
        	newPassword = md5(newPassword);

        	var sql = "SELECT `user_id` FROM `users` WHERE `username`=? AND `password`=?";
        	connection.query(sql, [username, oldPassword], function (err, response) {

        		if (err) {
        			sendResponse.somethingWentWrongError(res);
        		} else if (response.length > 0) {

        			var sql = "UPDATE `users` SET `password`=? WHERE `user_id`=?";
        			connection.query(sql, [newPassword, response[0].user_id], function (err, updateResponse) {

        				if (err) {
        					sendResponse.somethingWentWrongError(res);
        				} else {
        					sendResponse.sendSuccessData("Password updated successfully.", [], res);
        				}
        			});
        		} else {
        			sendResponse.sendSuccessData("Incorrect old password.", [], res);
        		}
        	});

        });
};


exports.searchApp = function (req, res) {

    var appName = req.body.app_name;

    var manValues = [appName];

    async.waterfall ([
        function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        }], function () {

            var sql = "SELECT (`cpu_usage`/`total_users`) AS average_cpu, (`data_usage`/`total_users`) AS average_data, `total_users` FROM `app_data` WHERE `app_name`=?";
            connection.query(sql, [appName], function (err, response) {

                if (err) {
                    sendResponse.somethingWentWrongError(res);
                } else if (response.length == 0) {
                    sendResponse.sendSuccessData("No results found.", [], res);
                } else {
                    sendResponse.sendSuccessData("", response, res);
                }
            });

        });

};