/**
 * The node-module to hold the constants for the server
 */


function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value: value,
        enumerable: true,
        writable: false,
        configurable: false
    });
}
exports.responseErrors = {};

define(exports.responseErrors, "EMAIL_NOT_EXISTS", "Email not registered");
define(exports.responseErrors, "FB_UNAUTHORIZATION", 'Not an authenticated user.');
define(exports.responseErrors, "ACCOUNT_BLOCKED", "Your account is blocked. Contact Admin for further details.");
define(exports.responseErrors, "ACCOUNT_DELETED", "Your account is deleted. Contact Admin for further details.");
define(exports.responseErrors, "WRONG_EMAIL_PASSWORD", "The email or password you entered is incorrect.");
define(exports.responseErrors, "EMAIL_ALREADY_REGISTERED", 'Email already exists.');
define(exports.responseErrors, "EMAIL_ALREADY_IN_USE", 'Email already in use.');
define(exports.responseErrors, "MANDATORY_FIELDS", {
    "error": 'Please provide all the information.',
    status: "false",
    "flag": 0
});
define(exports.responseErrors, "INVALID_ACCESS", {
    "error": 'Please login again, invalid access.',
    status: "false",
    "flag": 1
});
define(exports.responseErrors, "SOMETHING_WRONG", {
    "error": 'Something went wrong, try again.',
    status: "false",
    "flag": 0
});
define(exports.responseErrors, "BOOKING_CANCELLED", {
    "error": 'Booking cancelled.',
    status: "false",
    "flag": 2
});
define(exports.responseErrors, "INCORRECT_OLD_PASSWORD", 'Incorrect old password.');
define(exports.responseErrors, "COMING_SOON", 'Coming soon to your area please choose other location.');
define(exports.responseErrors, "SAME_TIME_BOOKING", 'Can\'t make booking as you already have other booking at same time.');
define(exports.responseErrors, "BOOKING_NOT_EXIST", 'Booking doesn\'t exist.');
define(exports.responseErrors, "CANT_CANCEL_BOOKING", 'Booking can\'t be cancelled now.');
define(exports.responseErrors, "CANT_ADD_CARD", 'Can\'t add more than five cards.');
define(exports.responseErrors, "INVALID_STRIPE_TOKEN", 'Invalid stripe token.');
define(exports.responseErrors, "PROMO_CODE_APPLIED_BEFORE", 'Promotion code already applied before.');
define(exports.responseErrors, "INCORRECT_PROMO_CODE", 'Incorrect promotion code.');
define(exports.responseErrors, "INVALID_TOKEN", 'Invalid token.');
define(exports.responseErrors, "PASSWORD_NOT_SET", 'Password not set.');
define(exports.responseErrors, "SHOW_FB_REGISTER_PAGE", 'Show Register Page');


exports.responseSuccess = {};

define(exports.responseSuccess, "WAIT_MESSAGE", {"message": "Thanks for registration. Please wait till the time admin approves your request."});
define(exports.responseSuccess, "PROFILE_UPDATED", "Profile updated successfully.");
define(exports.responseSuccess, "PARTY_BOOKED", {"message": "Party booked successfully."});
define(exports.responseSuccess, "BOOKING_MADE", {"message": "Booking made successfully."});
define(exports.responseSuccess, "PASSWORD_CHANGED_SUCCESSFULLY", {"message": "Password updated successfully."});
define(exports.responseSuccess, "BOOKING_CANCELLED", {"message": "Booking cancelled successfully"});
define(exports.responseSuccess, "BOOKINGS_CANCELLED", {"message": "Bookings cancelled successfully"});
define(exports.responseSuccess, "PROMOTION_CODE_APPLIED", "Promotion code applied successfully.");
define(exports.responseSuccess, "DEFAULT_CARD_CHANGED", {"message": "Default card changed successfully."});
define(exports.responseSuccess, "USER_LOGOUT_SUCCESSFULLY", {"message": "Logged out successfully."});
define(exports.responseSuccess, "CARD_DELETED", "Card deleted successfully.");
define(exports.responseSuccess, "FEEDBACK_SENT", {"message": "Feedback sent successfully."});
define(exports.responseSuccess, "CARD_ADDED", "Card added Successfully.");
define(exports.responseSuccess, "RATING_DONE", {"message": "Rating done successfully."});
define(exports.responseSuccess, "NO_BOOKING_DURING_CRON", {"message": "No bookings."});
define(exports.responseSuccess, "DONE_MESSAGE", {"message": "Done"});
define(exports.responseSuccess, "CHANGE_PASSWORD", {"message": "Check your email to change your password."});
define(exports.responseSuccess, "PASSWORD_RESET_SUCCESSFULLY", {"message": "Password reset successfully."});


exports.userRequestFrom = {};
define(exports.userRequestFrom, "EMAIL", 1);
define(exports.userRequestFrom, "FACEBOOK", 2);
define(exports.userRequestFrom, "BOTH", 3);

exports.userDeviceType = {};
define(exports.userDeviceType, "ANDROID", 1);
define(exports.userDeviceType, "IOS", 2);

exports.userRegistrationStatus = {};
define(exports.userRegistrationStatus, "CUSTOMER", 1);
define(exports.userRegistrationStatus, "DRIVER", 2);

exports.isBlocked = {};
define(exports.isBlocked, "NON_BLOCKED", 0);
define(exports.isBlocked, "BLOCKED", 1);
define(exports.isBlocked, "DELETED", 2);

exports.statesOfUser = {};
define(exports.statesOfUser, "HOME_SCREEN", 0);
define(exports.statesOfUser, "ON_THE_WAY", 1);
define(exports.statesOfUser, "SERVICE_STARTED", 2);
define(exports.statesOfUser, "END", 3);

exports.bookingStatuses = {};
define(exports.bookingStatuses, "NO_ACTION", 0);
define(exports.bookingStatuses, "CONFIRMED", 1);
define(exports.bookingStatuses, "OUT", 2);
define(exports.bookingStatuses, "REACHED", 3);
define(exports.bookingStatuses, "STARTED", 4);
define(exports.bookingStatuses, "ENDED", 5);
define(exports.bookingStatuses, "REJECTED", 6);
define(exports.bookingStatuses, "CANCELLED", 7);


exports.bookingOccurenceType = {};
define(exports.bookingOccurenceType, "ONCE", 1);
define(exports.bookingOccurenceType, "DAILY", 2);
define(exports.bookingOccurenceType, "WEEKLY", 3);
define(exports.bookingOccurenceType, "MONTHLY", 4);

exports.bookingRequestStatuses = {};
define(exports.bookingRequestStatuses, "NO_ACTION", 0);
define(exports.bookingRequestStatuses, "ACCEPTED", 1);
define(exports.bookingRequestStatuses, "REJECTED", 2);
define(exports.bookingRequestStatuses, "OTHER_ACCEPTED", 3);
define(exports.bookingRequestStatuses, "PAYMENT_ISSUE", 4);
define(exports.bookingRequestStatuses, "CANCELLED", 5);

exports.serviceType = {};
define(exports.serviceType, "HAIR", 1);
define(exports.serviceType, "MAKEUP", 2);
define(exports.serviceType, "BOTH", 3);

exports.driverVerified = {};
define(exports.driverVerified, "VERIFIED", 1);
define(exports.driverVerified, "NOT_VERIFIED", 0);


exports.generalVariables = {};
define(exports.generalVariables, "BUFFER_TIME", 30);
define(exports.generalVariables, "BOOKING_DAYS", 14);
define(exports.generalVariables, "LOCK_BOOKING_TIME", 3600000);   // in milliseconds
define(exports.generalVariables, "ONE_WEEK_CRON_START_TIME", 10080); // in minutes
define(exports.generalVariables, "ONE_WEEK_CRON_END_TIME", 10140);  // in minutes
define(exports.generalVariables, "CARDS_ALLOWED", 5);
define(exports.generalVariables, "TWO_HOUR_PUSH_CRON_START", 118);
define(exports.generalVariables, "TWO_HOUR_PUSH_CRON_END", 122);
define(exports.generalVariables, "STRIPE_MIN_VALUE", 0);
define(exports.generalVariables, "SERVICE_TIP", 0.2);
define(exports.generalVariables, "SERVICE_FEE", 0.4);

exports.generalMessages = {};
define(exports.generalMessages, "POPUP_MESSAGE", 'Update the app with new version.');
define(exports.generalMessages, "DEFAULT_LOGO", "logo.jpeg");


exports.pushMessages = {};
define(exports.pushMessages, "NEW_REQUEST", 'New request arrived.');
define(exports.pushMessages, "BOOKING_CANCELLED", 'Booking cancelled by user');
define(exports.pushMessages, "TWO_HOUR_BEFORE_PUSH", 'You have a service coming up in two hours.');

exports.pushFlags = {};
define(exports.pushFlags, "NEW_REQUEST", 1);
define(exports.pushFlags, "BOOKING_CANCELLED", 2);
define(exports.pushFlags, "TWO_HOUR_BEFORE_PUSH", 3);

