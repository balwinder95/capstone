var mysql = require('mysql'),
    DBSettings = require('../config/localDev.json').DBSettings;

//database connection

connection = mysql.createConnection({
    host: DBSettings.host,
    user: DBSettings.user,
    password: DBSettings.password,
    database: DBSettings.database,
    port: 3306
});
connection.connect();




