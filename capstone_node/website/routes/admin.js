var func = require('./commonfunction');
var constants = require('./constants');
var sendResponse = require('./sendResponse');
var md5 = require('MD5');
var async = require('async');


exports.adminLogin = function (req, res) {

	var username = req.body.username;
	var password = req.body.password;

	var manValues = [username, password];

	async.waterfall ([
		function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        }], function () {

        	password = md5(password);

        	var sql = "SELECT `admin_id` FROM `admin` WHERE `username`=? AND `password`=?";
        	connection.query(sql, [username, password], function (err, response) {

        		if (err) {
        			sendResponse.somethingWentWrongError(res);
        		} else if (response.length > 0) {
        			sendResponse.sendSuccessData("Login successful.", [], res);
        		} else {
        			sendResponse.sendSuccessData("Incorrect username or password.", [], res);
        		}
        	});

        });

};


exports.changePassword = function (req, res) {

	var username = req.body.username;
	var oldPassword = req.body.old_password;
	var newPassword = req.body.new_password;

	var manValues = [username, oldPassword, newPassword];

	async.waterfall ([
		function(callback) {
            func.checkBlankWithCallback(res, manValues, callback);
        }], function () {

        	oldPassword = md5(oldPassword);
        	newPassword = md5(newPassword);

        	var sql = "SELECT `admin_id` FROM `admin` WHERE `username`=? AND `password`=?";
        	connection.query(sql, [username, oldPassword], function (err, response) {

        		if (err) {
        			sendResponse.somethingWentWrongError(res);
        		} else if (response.length > 0) {

        			var sql = "UPDATE `admin` SET `password`=? WHERE `admin_id`=?";
        			connection.query(sql, [newPassword, response[0].admin_id], function (err, updateResponse) {

        				if (err) {
        					sendResponse.somethingWentWrongError(res);
        				} else {
        					sendResponse.sendSuccessData("Password updated successfully.", [], res);
        				}
        			});
        		} else {
        			sendResponse.sendSuccessData("Incorrect old password.", [], res);
        		}
        	});

        });
};

exports.cpuUsage = function (req, res) {

	var sql = "SELECT `app_name`, (`cpu_usage`/`total_users`) AS average_cpu, `total_users` FROM `app_data` ORDER BY average_cpu DESC";
	connection.query(sql, [], function (err, response) {
		if (err) {
			sendResponse.somethingWentWrongError(res);
			console.log(err);
		} else {
			sendResponse.sendSuccessData("", response, res);
		}
	});

};

exports.dataUsage = function (req, res) {

	var sql = "SELECT `app_name`, (`data_usage`/`total_users`) AS average_data, `total_users` FROM `app_data` ORDER BY average_data DESC";
	connection.query(sql, [], function (err, response) {
		if (err) {
			sendResponse.somethingWentWrongError(res);
			console.log(err);
		} else {
			sendResponse.sendSuccessData("", response, res);
		}
	});

};

exports.cpuGraph = function (req, res) {

    var sql = "SELECT `app_name`, `cpu_usage`, (`cpu_usage`/`total_users`) AS average_cpu FROM `app_data` ORDER BY average_cpu DESC";
    connection.query(sql, [], function (err, response) {
        if (err) {
            sendResponse.somethingWentWrongError(res);
            console.log(err);
        } else {

            var responseLength = response.length;
            var totalCpu = 0;
            var others = 0;
            var data = [];

            for(var i=0;i<responseLength;i++)
                totalCpu += response[i].cpu_usage;

            for(var i=5;i<responseLength;i++)
                others += response[i].cpu_usage;

            for(var i=0;i<5;i++) {
                data.push({
                    'app_name': response[i].app_name,
                    'cpu_usage': parseFloat(response[i].cpu_usage*100/totalCpu).toFixed(2)
                });
            }

            data.push({
                'app_name': 'others',
                'cpu_usage': parseFloat(others*100/totalCpu).toFixed(2)
            });

            sendResponse.sendSuccessData("", data, res);
        }
    });
};


exports.dataGraph = function (req, res) {

    var sql = "SELECT `app_name`, `data_usage`, (`data_usage`/`total_users`) AS average_data FROM `app_data` ORDER BY average_data DESC";
    connection.query(sql, [], function (err, response) {
        if (err) {
            sendResponse.somethingWentWrongError(res);
            console.log(err);
        } else {

            var responseLength = response.length;
            var totalData = 0;
            var others = 0;
            var data = [];

            for(var i=0;i<responseLength;i++)
                totalData += response[i].data_usage;

            for(var i=5;i<responseLength;i++)
                others += response[i].data_usage;

            for(var i=0;i<5;i++) {
                data.push({
                    'app_name': response[i].app_name,
                    'data_usage': parseFloat(response[i].data_usage*100/totalData).toFixed(2)
                });
            }

            data.push({
                'app_name': 'others',
                'data_usage': parseFloat(others*100/totalData).toFixed(2)
            });

            sendResponse.sendSuccessData("", data, res);
        }
    });
}