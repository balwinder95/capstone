var constants = require('./constants');
exports.invalidAccessTokenError = function (res) {
    var errorMsg = constants.responseErrors.INVALID_ACCESS;
    sendData(errorMsg, res);
};

exports.parameterMissingError = function (res) {

    var errorMsg = constants.responseErrors.MANDATORY_FIELDS;
    sendData(errorMsg, res);
};

exports.somethingWentWrongError = function (res) {

    var errorMsg = constants.responseErrors.SOMETHING_WRONG;
    sendData(errorMsg, res);
};

exports.bookingCancelled = function (res) {

    var errorMsg = constants.responseErrors.BOOKING_CANCELLED;
    sendData(errorMsg, res);
};

exports.sendSuccessData = function (msg, data, res) {

    var successData = {status: "true", message: msg, data: data};
    sendData(successData, res);
};

exports.sendError = function (error, res) {

    var successData = {status: "false", error: error, "flag": 0};
    sendData(successData, res);
};


exports.successStatusMsg = function (res) {

    var successMsg = {"status": "true"};
    sendData(successMsg, res);
};

exports.fbRegisterError = function (res) {

    var errorMsg = {"error": constants.responseErrors.SHOW_FB_REGISTER_PAGE, status: "false", flag: 10};
    sendData(errorMsg, res);
};

function sendData(data, res) {
    res.type('json');
    res.jsonp(data);
}


exports.sendData = function (data, res) {

    res.type('json');
    res.jsonp(data);
};


